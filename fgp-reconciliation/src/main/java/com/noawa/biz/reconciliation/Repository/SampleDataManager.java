package com.noawa.biz.reconciliation.Repository;

import com.noawa.biz.common.entities.BaseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by iPooya on 15/01/2018.
 */
public class SampleDataManager implements IDataManager {

    private static final Logger log = LoggerFactory.getLogger(SampleDataManager.class);
    @Autowired
    public DataSource dataSource;

    public SampleDataManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Object save(List<? extends BaseData> list) {
        JdbcBatchItemWriter<BaseData> writer = new JdbcBatchItemWriter<BaseData>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<BaseData>());
        writer.setSql("INSERT INTO base_data (cardNo, accountNo, stan, rrn, date) VALUES (:cardNo, :accountNo, :stan, :rrn, :date)");
        writer.setDataSource(dataSource);
        log.info("INSERT INTO base_data complete.");
        return writer;
    }

    @Override
    public void update(Object obiect) {

    }

    @Override
    public void delete(Object obiect) {

    }

}
