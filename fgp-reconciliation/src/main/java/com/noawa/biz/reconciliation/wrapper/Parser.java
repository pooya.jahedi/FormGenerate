package com.noawa.biz.reconciliation.wrapper;

import com.noawa.biz.common.entities.BaseData;

/**
 * Created by iPooya on 15/01/2018.
 */
public abstract class Parser implements IParser {
    protected Object inputData;
    protected Parser(Object inputData) {
        this.inputData = inputData;
    }

    public void setInputData(Object inputData) {
        this.inputData = inputData;
    }
    public abstract BaseData parse();
}
