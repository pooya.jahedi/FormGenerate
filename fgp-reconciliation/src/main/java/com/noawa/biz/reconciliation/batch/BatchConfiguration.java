package com.noawa.biz.reconciliation.batch;

import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.reconciliation.reader.BaseDataReader;
import com.noawa.biz.reconciliation.reader.ExcelItemReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.ArrayList;

/**
 * Created by iPooya on 14/01/2018.
 */
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public DataSource dataSource;

    @Autowired
    public BaseDataReader baseDataReader;

    @Bean
    public BaseDataReader reader() {

        if (baseDataReader == null)
            baseDataReader = new ExcelItemReader();
        return baseDataReader;


//        FlatFileItemReader<BaseData> reader = new FlatFileItemReader<BaseData>();
//        reader.setResource(new ClassPathResource("sample-data.csv"));
//        reader.setLineMapper(new DefaultLineMapper<BaseData>() {{
//            setLineTokenizer(new DelimitedLineTokenizer() {{
//                setNames(new String[] { "cardNo", "accountNo", "stan" , "rrn" , "date" });
//            }});
//            setFieldSetMapper(new BeanWrapperFieldSetMapper<BaseData>() {{
//                setTargetType(BaseData.class);
//            }});
//        }});
//        return reader;
    }

    @Bean
    public BaseDataItemProcessor processor() {
        return new BaseDataItemProcessor();
    }

    @Bean
    public BaseDataWriter<BaseData> writer() {
        BaseDataWriter baseDataWriter = new BaseDataWriter(dataSource);
        baseDataWriter.write(new ArrayList());
        return baseDataWriter;
    }

    @Bean
    public Job importBankJob(JobCompletionNotificationListener listener) {
        return jobBuilderFactory.get("importBankJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(stepManager())
                .end()
                .build();
    }

    @Bean
    public Step stepManager() {
        return stepBuilderFactory.get("stepManager")
                .<Object, BaseData> chunk(10000)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }

}
