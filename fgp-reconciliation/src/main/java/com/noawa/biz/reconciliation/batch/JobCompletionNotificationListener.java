package com.noawa.biz.reconciliation.batch;

/**
 * Created by iPooya on 14/01/2018.
 */

import com.noawa.biz.common.entities.BaseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

    private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public JobCompletionNotificationListener(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("!!! JOB FINISHED! Time to verify the results");

            List<BaseData> results = jdbcTemplate.query("SELECT cardno, accountno, stan, rrn, date FROM base_data", new RowMapper<BaseData>() {
                @Override
                public BaseData mapRow(ResultSet rs, int row) throws SQLException {
                    return new BaseData(rs.getString(1), rs.getString(2));
                }
            });

            for (BaseData baseData : results) {
                log.info("Found <" + baseData + "> in the database.");
            }

        }
    }
}
