package com.noawa.biz.reconciliation.batch;

import com.noawa.biz.common.exceptions.FGPException;
import com.noawa.biz.reconciliation.Repository.IDataManager;
import com.noawa.biz.reconciliation.Repository.SampleDataManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by iPooya on 15/01/2018.
 */
public class BaseDataWriter<BaseData> implements ItemWriter<BaseData>, InitializingBean {

    protected static final Log logger = LogFactory.getLog(BaseDataWriter.class);

    @Autowired
    public DataSource dataSource;
    IDataManager dataManager;

    public BaseDataWriter(DataSource dataSource) {
        if (dataManager == null)
            dataManager = new SampleDataManager(dataSource);
    }

    public IDataManager getDataManager() {
        return dataManager;
    }

    public void setDataManager(IDataManager dataManager) {
        this.dataManager = dataManager;
    }

//    @Override
//    public void write(List<? extends BaseData> list) throws Exception {
//        if (dataManager == null)
//            throw new FGPException("Invalid data manager value.");
//        dataManager.save(list);
//    }


    @Override
    public void afterPropertiesSet() {

    }

    @Override
    public void write(List<? extends BaseData> items)  {
        if(!items.isEmpty()) {
            if(logger.isDebugEnabled()) {
                logger.debug("Executing batch with " + items.size() + " items.");
            }

            try {

                dataManager.save((List<? extends com.noawa.biz.common.entities.BaseData>) items);
            } catch (FGPException e) {
                e.printStackTrace();
            }
        }
    }
}
