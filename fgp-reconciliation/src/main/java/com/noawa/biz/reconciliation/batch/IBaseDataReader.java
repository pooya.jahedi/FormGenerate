package com.noawa.biz.reconciliation.batch;

import com.noawa.biz.common.entities.MicroSystemConfiguration;
import org.springframework.batch.core.ItemReadListener;

/**
 * Created by iPooya on 17/01/2018.
 */
public interface IBaseDataReader extends ItemReadListener<Object> {

    MicroSystemConfiguration getMicroSystemConfiguration();

    void setMicroSystemConfiguration(MicroSystemConfiguration microSystemConfiguration);

}
