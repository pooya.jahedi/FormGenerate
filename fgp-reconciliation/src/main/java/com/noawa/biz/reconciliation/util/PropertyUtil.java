package com.noawa.biz.reconciliation.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by iPooya on 21/01/2018.
 */

@PropertySource("classpath:reconciliationConstant.properties")
public class PropertyUtil {

    @Value("${reconciliation.stan}")
    public static String stan_Name;
    @Value("${reconciliation.rrn}")
    public static String rrn_Name;
    @Value("${reconciliation.date}")
    public static String date_Name;
    @Value("${reconciliation.cardNo}")
    public static String cardNo_Name;
    @Value("${reconciliation.accountNo}")
    public static String accountNo_Name;
    @Value("${reconciliation.toCard}")
    public static String toCard_Name;
    @Value("${reconciliation.toAccount}")
    public static String toAccount_Name;


    public static class BaseDataInformation{
        public final static String stan_Name        = "STAN";
        public final static String rrn_Name         = "RRN";
        public final static String date_Name        = "DATE";
        public final static String cardNo_Name      = "CARD_NO";
        public final static String accountNo_Name   = "ACCOUNT_NO" ;
        public final static String toCard_Name      = "TO_CARD";
        public final static String toAccount_Name   = "TO_ACCOUNT";
        public final static String[] keys = {"STAN","RRN","DATE","CARD_NO","ACCOUNT_NO","TO_CARD","TO_ACCOUNT"};
    }

}
