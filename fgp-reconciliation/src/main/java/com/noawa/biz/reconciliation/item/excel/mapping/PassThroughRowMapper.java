/*
 * Copyright 2006-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.noawa.biz.reconciliation.item.excel.mapping;


import com.fasterxml.jackson.databind.ser.Serializers;
import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.reconcilition.item.excel.RowMapper;
import com.noawa.biz.reconcilition.item.excel.RowSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class PassThroughRowMapper implements RowMapper<BaseData> {

    protected final Log logger = LogFactory.getLog(getClass());
    @Override
    public BaseData mapRow(final RowSet rs) throws Exception {
        return process(rs.getCurrentRow());
//        String[] buffer = rs.getCurrentRow();
//        logger.info("row mapped : " + buffer[0]);
//        return buffer;
    }

    private BaseData process(String[] row){
        BaseData baseData = new BaseData();
        baseData.setDate(row[20]);
        baseData.setStan(row[16]);
        baseData.setRrn(row[16]);
        logger.info("Base data ==> " + baseData.toString());
        return baseData;
    }

}
