package com.noawa.biz.reconciliation.Repository;

import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.common.exceptions.FGPException;

import java.util.List;

/**
 * Created by iPooya on 15/01/2018.
 */
public interface IDataManager {

    Object save(List<? extends BaseData> list) throws FGPException;

    void update(Object obiect) throws FGPException;

    void delete(Object obiect) throws FGPException;


}
