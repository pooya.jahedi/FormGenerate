package com.noawa.biz.reconciliation.reader;

import org.springframework.batch.item.file.BufferedReaderFactory;
import org.springframework.batch.item.support.AbstractItemCountingItemStreamItemReader;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;

/**
 * Created by iPooya on 21/01/2018.
 */
public abstract class BaseDataReader extends AbstractItemCountingItemStreamItemReader<Object>
{
    protected Resource resource;
    protected String encoding;
    protected boolean isFinished;
    protected BufferedReader reader;
    protected BufferedReaderFactory bufferedReaderFactory;
}
