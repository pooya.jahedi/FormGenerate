package com.noawa.biz.reconciliation.wrapper;

import com.noawa.biz.common.entities.BaseData;

/**
 * Created by iPooya on 15/01/2018.
 */
public interface IParser {

    void setInputData(Object inputData);

    BaseData parse();
}
