package com.noawa.biz.reconciliation.batch;

import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.reconciliation.wrapper.IParser;
import com.noawa.biz.reconciliation.wrapper.SampleParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

/**
 * Created by iPooya on 15/01/2018.
 */
public class BaseDataItemProcessor  implements ItemProcessor<Object, BaseData>
{
        private static final Logger log = LoggerFactory.getLogger(BaseDataItemProcessor.class);
        IParser parser;

        public IParser getParser() {
            return parser;
        }

        public void setParser(IParser parser) {
            this.parser = parser;
        }

        @Override
        public BaseData process(final Object inputData) {
            if (parser == null) {
                parser = new SampleParser(inputData);
//                throw new FGPException("Invalid parser content.");
            }
            parser.setInputData(inputData);
            BaseData baseData = parser.parse();
            log.info("Converting (" + inputData + ") into (" + baseData + ")");
            return baseData;
        }


}
