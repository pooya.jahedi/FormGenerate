package com.noawa.biz.reconciliation.wrapper;

import com.noawa.biz.common.entities.BaseData;

/**
 * Created by iPooya on 15/01/2018.
 */
public class SampleParser extends Parser {


    public SampleParser(Object inputData) {
        super(inputData);
    }

    @Override
    public BaseData parse() {
        return new BaseData();
    }
}
