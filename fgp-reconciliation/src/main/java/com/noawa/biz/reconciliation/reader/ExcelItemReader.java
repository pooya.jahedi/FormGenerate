package com.noawa.biz.reconciliation.reader;


import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.reconciliation.item.excel.AbstractExcelItemReader;
import com.noawa.biz.reconciliation.item.excel.mapping.PassThroughRowMapper;
import com.noawa.biz.reconciliation.item.excel.poi.PoiItemReader;
import com.noawa.biz.reconcilition.item.excel.RowCallbackHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ClassUtils;

/**
 * Created by iPooya on 25/01/2018.
 */

public  class ExcelItemReader extends BaseDataReader{
    public ExcelItemReader() {
        setName(ClassUtils.getShortName(ExcelItemReader.class));
    }

    protected final Log logger = LogFactory.getLog(this.getClass());
    protected AbstractExcelItemReader itemReader;
    private ExecutionContext executionContext;

    public void setup() throws Exception {
        this.itemReader = createExcelItemReader();
        this.itemReader.setLinesToSkip(1); //First line is column names
        this.itemReader.setResource(new ClassPathResource("sampleLog.xlsx"));
        this.itemReader.setRowMapper(new PassThroughRowMapper());
        this.itemReader.setSkippedRowsCallback(new RowCallbackHandler() {

            public void handleRow(com.noawa.biz.reconcilition.item.excel.RowSet rs) {
//                logger.info("Skipping: " + StringUtils.arrayToCommaDelimitedString(rs.getCurrentRow()));
            }
        });
        configureItemReader(this.itemReader);
        this.itemReader.afterPropertiesSet();
        executionContext = new ExecutionContext();
        this.itemReader.open(executionContext);
    }

    protected void configureItemReader(AbstractExcelItemReader itemReader) {
    }


    public void after() {

    }

    public void readExcelFile() throws Exception {
        BaseData row;
        do {
            row = (BaseData) this.itemReader.read();
            this.logger.debug("Read: " + row);
            if (row != null) {
                //todo :
            }
        } while (row != null);
    }

    protected AbstractExcelItemReader createExcelItemReader() {
        return new PoiItemReader();
    }

    @Override
    protected Object doRead() throws Exception {
        readExcelFile();
        return null;
    }

    @Override
    protected void doOpen() throws Exception {
        setup();
    }

    @Override
    protected void doClose() {
        this.itemReader.close();
    }
}
