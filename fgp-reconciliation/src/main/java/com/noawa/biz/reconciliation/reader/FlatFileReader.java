package com.noawa.biz.reconciliation.reader;

import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.common.entities.MicroSystemConfiguration;
import com.noawa.biz.common.entities.RepositoryType;
import com.noawa.biz.common.exceptions.FGPException;
import com.noawa.biz.reconciliation.batch.IBaseDataReader;
import com.noawa.biz.reconciliation.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ClassUtils;

import java.util.HashMap;

/**
 * Created by iPooya on 20/01/2018.
 */
@PropertySource("reconciliationConstant.properties")
public class FlatFileReader extends BaseDataReader {


    public FlatFileReader() {
        setName(ClassUtils.getShortName(FlatFileReader.class));
    }

    private final Logger logger = LoggerFactory.getLogger(IBaseDataReader.class);
    private MicroSystemConfiguration microSystemConfiguration;
    private FlatFileItemReader<BaseData> itemReader;

    public MicroSystemConfiguration getMicroSystemConfiguration() {
        return microSystemConfiguration;
    }
    public void setMicroSystemConfiguration(MicroSystemConfiguration microSystemConfiguration) {
        this.microSystemConfiguration = microSystemConfiguration;
    }
    public FlatFileItemReader<BaseData> getItemReader() {
        return itemReader;
    }
    public void setItemReader(FlatFileItemReader<BaseData> itemReader) {
        this.itemReader = itemReader;
    }

    @Override
    protected Object doRead() throws Exception {
        return read();
    }

    @Override
    protected void doOpen() {
        if(microSystemConfiguration == null){
            HashMap<String, String> map = new HashMap<>();
            map.put(PropertyUtil.BaseDataInformation.stan_Name,"");
            map.put(PropertyUtil.BaseDataInformation.rrn_Name,"");
            map.put(PropertyUtil.BaseDataInformation.date_Name,"");
            map.put(PropertyUtil.BaseDataInformation.cardNo_Name,"");
            map.put(PropertyUtil.BaseDataInformation.accountNo_Name,"");
            map.put(PropertyUtil.BaseDataInformation.toCard_Name,"");
            map.put(PropertyUtil.BaseDataInformation.toAccount_Name,"");

            microSystemConfiguration = new MicroSystemConfiguration(
                    "sample","sample-data.csv", RepositoryType.csvFile,true,map);
        }

    }

    @Override
    protected void doClose() {

    }

    public FlatFileItemReader<BaseData> read() throws FGPException {
        if (microSystemConfiguration.getParameters() == null)
            throw new FGPException("invalid Parameters in microsystem.");
        String fileName  = microSystemConfiguration.getPath();
        itemReader = new FlatFileItemReader<BaseData>();
        itemReader.setResource(new ClassPathResource(fileName));
        itemReader.setLineMapper(new DefaultLineMapper<BaseData>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames((PropertyUtil.BaseDataInformation.keys));
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<BaseData>() {{
                setTargetType(BaseData.class);
            }});
        }});
        return itemReader;
    }

}
