DROP TABLE base_data IF EXISTS;

CREATE TABLE base_data  (
    data_id BIGINT IDENTITY NOT NULL PRIMARY KEY,
    cardNO VARCHAR(20),
    accountNO VARCHAR(20),
    stan VARCHAR(20),
    rrn VARCHAR(20),
    date VARCHAR(20)
);