package com.noawa.biz.common.form;

import com.noawa.biz.common.exceptions.FGPException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyHelper {

    public static Properties formRules;

    public static Properties load(String url) throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = new FileInputStream(url);
        properties.load(inputStream);
        return properties;
    }

    static
    {
        try{
        formRules = load("classPath: formRules.properties");

        }catch (IOException ex){
            ex.printStackTrace();
        }

    }

    public static void main(String[] args) {
        PropertyHelper propertyHelper = new PropertyHelper();
        System.out.printf("tttt ====" + propertyHelper.formRules.getProperty("transaction.cash"));
    }


}
