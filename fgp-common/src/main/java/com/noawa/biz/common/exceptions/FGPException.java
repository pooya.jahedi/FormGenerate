package com.noawa.biz.common.exceptions;

/**
 * Created by iPooya on 15/01/2018.
 */
public class FGPException extends Exception {

    public FGPException() {
    }

    public FGPException(String message) {
        super(message);
    }

    public FGPException(String message, Throwable cause) {
        super(message, cause);
    }

    public FGPException(Throwable cause) {
        super(cause);
    }

    public FGPException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
