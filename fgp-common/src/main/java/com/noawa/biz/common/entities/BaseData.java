package com.noawa.biz.common.entities;

/**
 * Created by iPooya on 14/01/2018.
 */
public class BaseData {

    private String cardNo;
    private String accountNo;
    private String stan;
    private String rrn;
    private String date;
    private String Time;
    private Double Amount;
    private String AmountType;
    private String StatementCode;
    private String TerminalID;
    private String RRNCode;
    private String BranchCode;
    private String TRXStatus;
    private String AquireBranchcode;
    private String AquireBankCode;
    private String ToCard;
    private String ToAccountNo;
    private String operationCode;
    private String AquireBin;
    private String IssuerBin;
    private String TRXTypeCode;
    private String WageAmount;
    private String DeviceType;
    private String ResponseCode;
    private String FinancialDocCode;
    private String FinancialDocDate;

    public BaseData() {
    }

    public BaseData(String stan, String rrn) {
        this.stan = stan;
        this.rrn = rrn;
    }


    //
    // this Class Creator use for Form 13 Creation
    //

    public BaseData( String issuerBin , String TRXTypeCode, String deviceType , String Amount,String amountType ) {
        this.IssuerBin = issuerBin;
        this.DeviceType = deviceType  ;
        this.AmountType =amountType ;
        this.Amount=Double.parseDouble(Amount);
        this.TRXTypeCode  =TRXTypeCode;
    }
    //
    // this Class Creator use for Form 9 Creation
    //

    public BaseData( String accountNo, String TRXTypeCode, String date, String Time, String stan,String cardNo, String  IssuerBin, String TerminalID, String Amount,
                     String DeviceType, String ToAcountNo, String ToCard, String WageAmount, String AquireBankCode, String ResponseCode,
                     String FinancialDocCode, String FinancialDocDate) {
        this.stan = stan;
        this.cardNo = cardNo ;
        this.accountNo = accountNo;
        this.date=date;
        this.Amount=Double.parseDouble(Amount);
        this.TerminalID=TerminalID;
        this.AquireBankCode=AquireBankCode;
        this.ToCard =ToCard;
        this.ToAccountNo =ToAcountNo ;
        this.IssuerBin=IssuerBin;
        this.TRXTypeCode  =TRXTypeCode;
        this.Time=Time;
        this.DeviceType=DeviceType;
        this.WageAmount = WageAmount ;
        this.ResponseCode=ResponseCode;
        this.FinancialDocCode=FinancialDocCode ;
        this.FinancialDocDate=FinancialDocDate;
    }

    //
    // this Class Creator use for Form 8 Creation
    //


    public BaseData( String cardNo, String accountNo, String stan, String rrn, String date,String Time, String Amount, String AmountType, String StatementCode,
                     String TerminalID, String RRNCode, String BranchCode, String TRXStatus, String AquireBranchcode, String AquireBankCode,
                      String CardNo3, String ToAccountNo, String operationCode, String AquireBin, String IssuerBin,String TRXtypeCode ) {
        this.stan = stan;
        this.rrn = rrn;
        this.cardNo = cardNo ;
        this.accountNo = accountNo;
        this.date=date;
        this.Amount=Double.parseDouble ( Amount);
        this.AmountType=AmountType;
        this.StatementCode=StatementCode;
        this.TerminalID=TerminalID;
        this.RRNCode=RRNCode;
        this.BranchCode=BranchCode;
        this.TRXStatus=TRXStatus;
        this.AquireBranchcode=AquireBranchcode;
        this.AquireBankCode=AquireBankCode;
        this.ToCard=CardNo3;
        this.ToAccountNo=ToAccountNo ;
        this.operationCode=operationCode;
        this.AquireBin=AquireBin;
        this.IssuerBin=IssuerBin;
        this.TRXTypeCode  =TRXTypeCode;
        this.Time=Time;
    }

    public Double getAmount() {
        return Amount;
    }

    public String getAmountType() {
        return AmountType;
    }

    public String getStatementCode() {
        return StatementCode;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getTerminalID() {
        return TerminalID;
    }

    public String getRRNCode() {
        return RRNCode;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public String getTRXStatus() {
        return TRXStatus;
    }

    public String getAquireBranchcode() {
        return AquireBranchcode;
    }

    public String getAquireBankCode() {
        return AquireBankCode;
    }

    public String getToCard() {
        return ToCard;
    }

    public String getToAccountNo()  {
        return ToAccountNo;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public String getAquireBin() {
        return AquireBin;
    }

    public String getIssuerBin() {
        return IssuerBin;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setAmount(Double amount) {
        Amount = amount;
    }

    public void setAmountType(String amountType) {
        AmountType = amountType;
    }

    public void setStatementCode(String statementCode) {
        StatementCode = statementCode;
    }

    public void setTerminalID(String terminalID) {
        TerminalID = terminalID;
    }

    public void setRRNCode(String RRNCode) {
        this.RRNCode = RRNCode;
    }

    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;
    }

    public void setTRXStatus(String TRXStatus) {
        this.TRXStatus = TRXStatus;
    }

    public void setAquireBranchcode(String aquireBranchcode) {
        AquireBranchcode = aquireBranchcode;
    }

    public void setAquireBankCode(String aquireBankCode) {
        AquireBankCode = aquireBankCode;
    }

    public void setToCard(String toCard) {
        ToCard = toCard;
    }

    public void setToAccountNo(String ToAccountNo) {
        this.ToAccountNo = ToAccountNo;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public void setAquireBin(String aquireBin) {
        AquireBin = aquireBin;
    }

    public void setIssuerBin(String issuerBin) {
        IssuerBin = issuerBin;
    }

    public String getTRXTypeCode() {
        return TRXTypeCode;
    }

    public void setTRXTypeCode(String TRXTypeCode) {
        this.TRXTypeCode = TRXTypeCode;
    }

    public String getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(String deviceType) {
        DeviceType = deviceType;
    }

    public String getWageAmount() {
        return WageAmount;
    }

    public void setWageAmount(String wageAmount) {
        WageAmount = wageAmount;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getFinancialDocCode() {
        return FinancialDocCode;
    }

    public void setFinancialDocCode(String financialDocCode) {
        FinancialDocCode = financialDocCode;
    }

    public String getFinancialDocDate() {
        return FinancialDocDate;
    }

    public void setFinancialDocDate(String financialDocDate) {
        FinancialDocDate = financialDocDate;
    }


}
