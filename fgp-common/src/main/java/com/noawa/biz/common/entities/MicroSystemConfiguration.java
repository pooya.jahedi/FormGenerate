package com.noawa.biz.common.entities;

import java.util.Map;

/**
 * Created by iPooya on 15/01/2018.
 */
public class MicroSystemConfiguration {

    Long id;
    String name;
    String path;
    RepositoryType repositoryType;
    boolean isActive;
    private Map<String, String> parameters;

    public MicroSystemConfiguration(String name, String path, RepositoryType repositoryType, boolean isActive,
                                    Map parameters) {
        this.name = name;
        this.path = path;
        this.repositoryType = repositoryType;
        this.isActive = isActive;
        this.parameters = parameters;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public RepositoryType getRepositoryType() {
        return repositoryType;
    }

    public void setRepositoryType(RepositoryType repositoryType) {
        this.repositoryType = repositoryType;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
}
