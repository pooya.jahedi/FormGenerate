package com.noawa.biz.core.dataAccess;

import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.common.exceptions.FGPException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * Created by iPooya on 16/01/2018.
 */
public class ReadProcessor implements IReader{

    @Override
    public ArrayList<BaseData> read() throws URISyntaxException, IOException {
        ArrayList<BaseData> arraylist = new ArrayList<BaseData>();
        Path path = Paths.get(getClass().getClassLoader().getResource("fileTest.txt").toURI());
        StringBuilder data = new StringBuilder();
        Stream<String> lines = Files.lines(path);
        for (String item : (Iterable<String>) () -> lines.iterator()) {
            String [] spliteditems=item.split(";");
            arraylist.add(new BaseData(spliteditems[0],spliteditems[1],spliteditems[2],spliteditems[3],spliteditems[4]));
        }
        lines.close();
        return arraylist;
    }

    @Override
    public BaseData read(Long id) throws FGPException {
        return null;
    }
}
