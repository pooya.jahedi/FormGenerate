package com.noawa.biz.core.generator;

import com.noawa.biz.common.date.JalaliCalendar;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class FormUtil {


    public static String getJalaliShortDate(String date){
        int year = Integer.parseInt(date.substring(0,4));
        int month = Integer.parseInt(date.substring(4,6));
        int day = Integer.parseInt(date.substring(6,8));
        String time = date.substring(8);
        String jalaliDate =  JalaliCalendar.gregorianToJalali(new JalaliCalendar.YearMonthDate(year, month, day)).toString();
        String[] temp = jalaliDate.split("/");
        if (temp.length == 3)
        {
            temp[1] = leftZeroPad(temp[1],2);
            temp[2] = leftZeroPad(temp[2], 2);
        }
        return (temp[0].concat(temp[1]).concat(temp[2]).concat(time)).substring(2);
    }

    public static String makePad(int len, String padType){
        String pad = "";
        for (int i=1;i<=len;i++)
            pad = pad.concat(padType);
        return pad;
    }

    public static int calculateDef(String value, int len){
        int valueLen = value.length();
        if (valueLen < len)
            len = len-valueLen;
        return len;
    }

    public static String leftZeroPad(String value, int len){
        return makePad(calculateDef(value,len),"0").concat(value);
    }

    public static String rightZeroPad(String value, int len){
        return value.concat(makePad(calculateDef(value,len),"0"));
    }

    public static String leftSpacePad(String value, int len){
        return makePad(calculateDef(value,len)," ").concat(value);
    }

    public static String rightSpacePad(String value, int len){
        return value.concat(makePad(calculateDef(value,len)," "));
    }

    public static void main(String[] args) {
        FormUtil formUtil = new FormUtil();
        String date  = "20180131142022";
        String dd = formUtil.getJalaliShortDate(date);
        System.out.println("args = [" + dd + "]");

        System.out.println("args = [" + formUtil.leftZeroPad("1234",10)+ "]");
        System.out.println("args = [" + formUtil.rightZeroPad("1234",10)+ "]");
        System.out.println("args = [" + formUtil.leftSpacePad("1234",10)+ "]");
        System.out.println("args = [" + formUtil.rightSpacePad("1234",10)+ "]");
    }
}
