package com.noawa.biz.core.dataAccess;

import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.common.exceptions.FGPException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by iPooya on 16/01/2018.
 */
public interface IReader {

    ArrayList<BaseData> read() throws FGPException, URISyntaxException, IOException;
    BaseData read(Long id) throws FGPException;
}
