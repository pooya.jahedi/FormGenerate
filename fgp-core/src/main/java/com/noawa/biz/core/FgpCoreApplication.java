package com.noawa.biz.core;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

@SpringBootApplication
@ComponentScan("com.noawa.biz.reconciliation")
@EntityScan(basePackages="com.noawa.biz.reconciliation")
public class FgpCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(FgpCoreApplication.class, args);
	}


//	@Bean
//	public CommandLineRunner run(ApplicationContext appContext) {
//		return args -> {
//
//			String[] beans = appContext.getBeanDefinitionNames();
//			Arrays.stream(beans).sorted().forEach(System.out::println);
//			com.noawa.biz.reconciliation.Bean.parser = appContext.getBean("")
//
//		};
//	}



}
