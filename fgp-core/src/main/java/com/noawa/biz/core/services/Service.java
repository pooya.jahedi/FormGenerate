package com.noawa.biz.core.services;

/**
 * Created by iPooya on 14/01/2018.
 */
public class Service {
    private String type;
    private int length;
    private int height;

    public Service(String type, int length, int height) {
        this.type = type;
        this.length = length;
        this.height = height;
    }

    public String getType() {
        return type;
    }

    public int getLength() {
        return length;
    }

    public int getHeight() {
        return height;
    }
}
