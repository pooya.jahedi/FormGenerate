package com.noawa.biz.core.generator;

import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.common.exceptions.FGPException;

import java.util.ArrayList;
import java.util.List;

public class Form9 extends Form {

    private static final String seprator = "|";

    @Override
    public void process() throws FGPException {
        String record = "";
        List<String> records = new ArrayList<>();
        for (Long id:getIds()) {
            BaseData baseData = getReader().read(id);
            record = makeRecord(baseData,id.toString() );
            records.add(record);
        }
        getWriter().write(records);
    }

    @Override
    public String makeRecord(BaseData baseData) {
        return null;
    }

    @Override
    public String makeRecord(BaseData baseData, String  id) {

        StringBuffer sb = new StringBuffer();
        //1
         sb.append(FormUtil.leftZeroPad(id,6));
        sb.append(seprator);
        //2
        sb.append(FormUtil.rightSpacePad(baseData.getAccountNo() ,13) );
        sb.append(seprator);
        //3
        sb.append(baseData.getTRXTypeCode() );
        sb.append(seprator);
        //4
        sb.append(FormUtil.getJalaliShortDate(baseData.getDate()));
        sb.append(seprator);
        //5
        sb.append(baseData.getTime() );
        sb.append(seprator);
        //6
        sb.append(FormUtil.leftZeroPad(  baseData.getStan() ,6));
        sb.append(seprator);
        //7
        sb.append(FormUtil.leftZeroPad(baseData.getCardNo() ,19) );
        sb.append(seprator);
        //8
        sb.append(baseData.getIssuerBin());
        sb.append(seprator);
        //9
        sb.append(FormUtil.leftZeroPad(baseData.getTerminalID(),8));
        sb.append(seprator);
        //10
        sb.append(FormUtil.leftZeroPad(  baseData.getAmount().toString() ,12));
        sb.append(seprator);
        //11
        sb.append(  baseData.getDeviceType());
        sb.append(seprator);
        //12
        sb.append(FormUtil.rightSpacePad(baseData.getToAccountNo() ,13) );
        sb.append(seprator);
        //13
        sb.append(FormUtil.leftZeroPad(baseData.getToCard() ,19) );
        sb.append(seprator);
        //14
        sb.append(baseData.getWageAmount());
        sb.append(seprator);
        //15
        sb.append(  baseData.getAquireBankCode());
        sb.append(seprator);
        //16
        sb.append(  baseData.getResponseCode());
        sb.append(seprator);
        //17
        sb.append("0");
        sb.append(seprator);
        //18
        sb.append("0");
        sb.append(seprator);
        //19
        sb.append(baseData.getFinancialDocCode());
        sb.append(seprator);
        //20
        sb.append(FormUtil.getJalaliShortDate(baseData.getFinancialDocDate()));
        sb.append(seprator);
        //21
        sb.append(endLine);

        return sb.toString();

    }
}
