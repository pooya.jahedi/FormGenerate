package com.noawa.biz.core.dataAccess;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by iPooya on 1/20/2018.
 */
public class WriteProcessor implements IWriter  {

    @Override
    public void write(List<String> record) {
        String inputFileName ="sampleData.txt";
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(inputFileName,true));
            for (String Content : record ) {
                bw.write(Content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(String inputFileName, String Contetnt) {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(inputFileName,true));
            bw.write(Contetnt) ;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
