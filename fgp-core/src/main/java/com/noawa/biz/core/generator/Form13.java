package com.noawa.biz.core.generator;

import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.common.exceptions.FGPException;

import java.util.ArrayList;
import java.util.List;

public class Form13 extends Form {

    private static final String seprator = "|";

    @Override
    public void process() throws FGPException {
        String record = "";
        List<String> records = new ArrayList<>();
        for (Long id:getIds()) {
            BaseData baseData = getReader().read(id);
            record = makeRecord(baseData);
            records.add(record);
        }
        getWriter().write(records);

    }

    @Override
    public String makeRecord(BaseData baseData,String id) {
        return null;
    }


    @Override
    public String makeRecord(BaseData baseData) {

        StringBuffer sb = new StringBuffer();
        //1
        sb.append(baseData.getIssuerBin() );
        sb.append(seprator);
        //2
        sb.append(baseData.getTRXTypeCode() );
        sb.append(seprator);
        //3
        sb.append(baseData.getDeviceType() );
        sb.append(seprator);
        //4
        sb.append(FormUtil.leftZeroPad(baseData.getAmount().toString(),15  ) );
        sb.append(seprator);
        //5
        sb.append(baseData.getAmountType() );
        sb.append(seprator);
        //6
        sb.append(endLine);

        return sb.toString();
    }
}
