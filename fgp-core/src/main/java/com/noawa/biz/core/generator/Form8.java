package com.noawa.biz.core.generator;

import com.noawa.biz.common.date.JalaliCalendar;
import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.common.exceptions.FGPException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Form8 extends Form{

    private static final String seprator = "|";


    @Override
    public void process() throws FGPException {
        String record = "";
        List<String> records = new ArrayList<>();
        for (Long id:getIds()) {
            BaseData baseData = getReader().read(id);
            record = makeRecord(baseData);
            records.add(record);
        }
        getWriter().write(records);
    }

    @Override
    public String makeRecord(BaseData baseData,String id) {
        return null;
    }
    @Override
    public String makeRecord(BaseData baseData) {

        String flag = "INTERNAL_TRANSFER";
        if(baseData.getTRXTypeCode().equalsIgnoreCase("46"))
            flag = "TRANSFER_FROM";
        else if(baseData.getTRXTypeCode().equalsIgnoreCase("47"))
            flag = "TRANSFER_TO";


        StringBuffer sb = new StringBuffer();



        //1
        String identifier = FormUtil.getJalaliShortDate(baseData.getDate());
        sb.append(identifier);
        sb.append(FormUtil.leftZeroPad(baseData.getTRXTypeCode(),2));
        sb.append(FormUtil.leftZeroPad(baseData.getStan(),6));
        sb.append(seprator);
        //2
        sb.append("0000");
        sb.append(seprator);
        //3
        sb.append("8888");
        sb.append(seprator);
        //4
        sb.append("O");
        sb.append(seprator);
        //5
        sb.append(baseData.getAccountNo());
        sb.append(seprator);
        //6
        sb.append(FormUtil.leftZeroPad(  baseData.getAmount().toString() ,12));
        sb.append(seprator);
        //7
        sb.append(  baseData.getAmountType());
        sb.append(seprator);
        //8
        sb.append(baseData.getStatementCode());
        sb.append(seprator);
        //9
        sb.append("0");
        sb.append(seprator);
        //10
        sb.append(FormUtil.leftZeroPad(  baseData.getTerminalID(),8));
        sb.append(seprator);
        //11
        sb.append(FormUtil.leftZeroPad(  baseData.getRRNCode(),12));
        sb.append(seprator);
        //12
        sb.append(  baseData.getBranchCode());
        sb.append(seprator);
        //13
        sb.append(  baseData.getTRXStatus());
        sb.append(seprator);
        //14
        sb.append(FormUtil.leftZeroPad(baseData.getStan(),6));
        sb.append(seprator);
        //15
        sb.append(  baseData.getAquireBranchcode());
        sb.append(seprator);
        //16
        sb.append(  baseData.getAquireBankCode());
        sb.append(seprator);
        //17
        sb.append("0000");
        sb.append(seprator);
        //18
        sb.append(FormUtil.leftZeroPad(baseData.getToCard() ,19));
        sb.append(seprator);
        //19
        sb.append(FormUtil.leftZeroPad(baseData.getCardNo(),19));
        sb.append(seprator);
        //20
        sb.append(FormUtil.leftZeroPad(baseData.getToCard(),19));
        sb.append(seprator);
        //21
        sb.append(FormUtil.leftZeroPad(baseData.getAccountNo(),13));
        sb.append(seprator);
        //22
        sb.append(FormUtil.leftZeroPad("0",13));
        sb.append(seprator);
        //23
        sb.append(  baseData.getOperationCode());
        sb.append(seprator);
        //24
        sb.append(  baseData.getAquireBin());
        sb.append(seprator);
        //25
        sb.append(  baseData.getIssuerBin());
        sb.append(seprator);
        //26
        sb.append("0");
        sb.append(seprator);
        //27
        sb.append("0");
        sb.append(seprator);
        //28
        sb.append(endLine);

        return sb.toString();

    }
}
