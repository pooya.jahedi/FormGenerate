package com.noawa.biz.core.generator;

import com.noawa.biz.common.entities.BaseData;
import com.noawa.biz.common.exceptions.FGPException;
import com.noawa.biz.core.dataAccess.IReader;
import com.noawa.biz.core.dataAccess.IWriter;

import java.io.Serializable;
import java.util.List;

public  abstract class Form  implements Serializable{

    private List<Long> ids;
    private IReader reader;
    private IWriter writer;
    protected static final String endLine = "\n";

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public IReader getReader() {
        return reader;
    }

    public void setReader(IReader reader) {
        this.reader = reader;
    }

    public IWriter getWriter() {
        return writer;
    }

    public void setWriter(IWriter writer) {
        this.writer = writer;
    }

    public abstract void process() throws FGPException;

    public abstract String makeRecord(BaseData baseData);

    public abstract String makeRecord(BaseData baseData,String Id);
}
