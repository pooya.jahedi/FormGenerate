package com.noawa.biz.core.dataAccess;

import java.util.List;

public interface IWriter {

    public void write(List<String> record);

    public void write(String inputFileName, String Contetnt);
}
